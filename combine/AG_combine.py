import csv
import numpy as np
import matplotlib.pyplot as plt
import os




file1 = open("../model_save/ag_output-2/test_results.tsv")
file2 = open("../model_save/ag_ori_output-2/test_results.tsv")



file3 = open("../glue_data/AG/test.csv")

reader1 = csv.reader(file1, delimiter="\t", quotechar=None)
p = []
for line in reader1:
    p.append([float(line[0]), float(line[1]), float(line[2]), float(line[3])])

reader2 = csv.reader(file2, delimiter="\t", quotechar=None)
p_ori = []
for line in reader2:
    p_ori.append([float(line[0]), float(line[1]), float(line[2]), float(line[3])])

# reader3 = csv.reader(file3, delimiter="\t", quotechar=None)
reader3 = csv.reader(file3)
t = []
for i, line in enumerate(reader3):
    # if i == 0:
    #     continue
    t.append(int(line[0]) - 1)

p = np.array(p)
p_ori = np.array(p_ori)
t = np.array(t)

y = np.argmax(p, 1)
y_ori = np.argmax(p_ori, 1)

is_ch = (y == t)
is_ori = (y_ori == t)

# print(t)
a = []
for i in range(1001):
    w = i / 1000
    p_new = w * p + (1 - w) * p_ori
    y = np.argmax(p_new, 1)

    # if np.sum(y == t) / len(y) > 0.901:
    #     print(w)
    a.append(np.sum(y == t) / len(y))



# for i in range(len(y)):
#     if is_ch[i] == 1 and is_ori[i] == 0:
#         count += 1
#         # print(i + 1)

# print(count/len(y))

print('A = ' + str(max(a)))
print('W = ' + str(np.argmax(a, 0) / 1000))

# print(len(a))
plt.rcParams['savefig.dpi'] = 300
plt.tick_params(labelsize=18)
plt.xlim(0,1)
plt.plot(np.linspace(0, 1, 1001), a, color='black')
plt.savefig('figure.eps')
plt.show()
count = 0