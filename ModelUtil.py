# coding=utf-8

import tensorflow as tf
import numpy as np
from tensorflow.python.ops.rnn import bidirectional_dynamic_rnn
from tensorflow.contrib.rnn import GRUCell


def cnn_layer(input, input_shape, filter_shape, strides_shape, name):
    with tf.variable_scope(name):
        image = tf.reshape(input, input_shape)
        weight = tf.get_variable('W', filter_shape, initializer=tf.truncated_normal_initializer(stddev=0.1))
        bias = tf.get_variable('B', [filter_shape[3]], initializer=tf.constant_initializer(0.1))
        return tf.tanh(tf.nn.conv2d(image, weight, strides=strides_shape, padding='SAME') + bias)


def max_pool_layer(input, filter_shape, name):
    with tf.variable_scope(name):
        return tf.nn.max_pool(input, ksize=filter_shape, strides=filter_shape, padding='VALID')


def bi_rnn_layer(input, input_shape, sequence_len, hidden_num, name):
    with tf.variable_scope(name):
        input_handel = tf.reshape(input, input_shape)
        output, _ = bidirectional_dynamic_rnn(GRUCell(hidden_num), GRUCell(hidden_num), inputs=input_handel,
                                              sequence_length=sequence_len, dtype=tf.float32)
        return output



def attention_layer(input, hidden_size, name):
    with tf.variable_scope(name):
        input_shape = input.shape
        sequence_len = input_shape[1].value
        encoding_len = input_shape[2].value

        weight = tf.get_variable('W', [encoding_len, hidden_size],
                                 initializer=tf.random_normal_initializer(mean=0, stddev=0.1))
        bias = tf.get_variable('b', [hidden_size], initializer=tf.random_normal_initializer(mean=0, stddev=0.1))
        u_omega = tf.get_variable('u', [hidden_size], initializer=tf.random_normal_initializer(mean=0, stddev=0.1))

        v = tf.tanh(tf.matmul(tf.reshape(input, [-1, encoding_len]), weight) + tf.reshape(bias, [1, -1]))
        vu = tf.matmul(v, tf.reshape(u_omega, [-1, 1]))
        exps = tf.reshape(tf.exp(vu), [-1, sequence_len])
        alphas = exps / tf.reshape(tf.reduce_sum(exps, 1), [-1, 1])
        output = tf.reduce_sum(input * tf.reshape(alphas, [-1, sequence_len, 1]), 1)

        return output, alphas

def multi_attention_layer(input, name):
    with tf.variable_scope(name):
        input_shape = input.shape
        sequence_length = input_shape[1].value
        encoding_length = input_shape[2].value

        # variable
        weight_hidden = tf.get_variable('Wh', [encoding_length, encoding_length], initializer=tf.random_normal_initializer(mean=0, stddev=0.1))
        # weight_j = tf.get_variable('Wj', [encoding_length, hidden_size], initializer=tf.randodm_normal_initalizer(mean=0, stddev=0.1))
        # weight_omega = tf.get_variable('Wo', [hidden_size, encoding_length], initializer=tf.random_normal_initializer(mean=0, stddev=0.1))
        bias_hidden = tf.get_variable('Bh', [encoding_length], initializer=tf.constant_initializer(0.1))
        # bias_omega = tf.get_variable('Bo', [encoding_length], initializer=tf.constant_initializer(0.1))

        # alpha compute
        h = tf.tanh(tf.matmul(tf.reshape(input, [-1, encoding_length]), weight_hidden) + tf.reshape(bias_hidden, [1, -1]))
        # o = tf.tanh(tf.matmul(h, weight_omega) + tf.reshape(bias_omega, [1, -1]))
        exps = tf.exp(tf.reshape(h, [-1, sequence_length, encoding_length]))
        alphas = exps / tf.reshape(tf.reduce_sum(exps, 1), [-1, 1, encoding_length])

        # s compute
        output = tf.reduce_sum(alphas * input, 1)

        return output, alphas

def cnn_attention_layer(input, conv_window, filter_num, name):
    cnn_output = cnn_layer(input, [-1, input.shape[1].value, input.shape[2].value, 1],
                           [conv_window, input.shape[2].value, 1, filter_num], [1, 1, input.shape[2].value, 1], name)
    Alpha_ori = tf.reshape(tf.reduce_mean(cnn_output, axis=3), [-1, input.shape[1].value])
    Alpha = Alpha_ori / tf.reshape(tf.reduce_sum(Alpha_ori, axis=1), [-1, 1])
    attention_output = tf.reduce_sum(input * tf.reshape(Alpha, [-1, input.shape[1].value, 1]), axis=1)
    return attention_output, Alpha

def combine_hf_x_hb(word_embedding, bi_rnn_sequence, batch_size):
    forward_sequence, affterward_sequence = bi_rnn_sequence
    zero_mat = tf.Variable(tf.constant(1e-10, shape=[batch_size, 1, forward_sequence.shape[2].value]))
    left_context = tf.slice(tf.concat([zero_mat, forward_sequence], axis=1), [0, 0, 0],
                            [-1, forward_sequence.shape[1].value, -1])
    right_context = tf.slice(tf.concat([affterward_sequence, zero_mat], axis=1), [0, 1, 0], [-1, -1, -1])
    X = tf.concat([tf.concat([left_context, word_embedding], axis=2), right_context], axis=2)
    return X


def batch_generator(X, y, batch_size):
    size = X.shape[0]
    X_copy = X.copy()
    y_copy = y.copy()
    indices = np.arange(size)
    np.random.shuffle(indices)
    X_copy = X_copy[indices]
    y_copy = y_copy[indices]
    i = 0
    while True:
        if i + batch_size <= size:
            yield X_copy[i:i + batch_size], y_copy[i:i + batch_size]
            i += batch_size
        else:
            i = 0
            indices = np.arange(size)
            np.random.shuffle(indices)
            X_copy = X_copy[indices]
            y_copy = y_copy[indices]
            continue


def average_gradients(grads):#grads:[[grad0, grad1,..], [grad0,grad1,..]..]
  averaged_grads = []
  for grads_per_var in zip(*grads):
    grads = []
    for grad in grads_per_var:
      expanded_grad = tf.expand_dims(grad, 0)
      grads.append(expanded_grad)
    grads = tf.concat(grads, axis=0)
    grads = tf.reduce_mean(grads, 0)
    averaged_grads.append(grads)

  return averaged_grads

# def drnn_mlp(input, input_shape, windows_size, hidden_num, keep_prob, name):
#     input_handel = tf.nn.dropout(tf.reshape(input, input_shape), keep_prob=keep_prob)
#
#     input_pad = tf.pad(input_handel, [[0, 0], [windows_size - 1, 0], [0, 0]])
#     state = []
#
#     with tf.name_scope(name):
#
#         for i in range(input_handel.shape[1]):
#             with tf.variable_scope('rnn') as scope:
#                 if i != 0:
#                     scope.reuse_variables()
#                 data_win = input_pad[:, i: i + windows_size - 1, :]
#                 hidden, laststate = tf.nn.dynamic_rnn(GRUCell(hidden_num), inputs=data_win, dtype=tf.float32)
#
#             laststate_dropout = tf.nn.dropout(laststate, keep_prob=keep_prob)
#             state.append(tf.reshape(laststate_dropout, [-1, 1, hidden_num]))
#
#         state_con = tf.concat(state, axis=1)
#         state_exp = tf.reshape(state_con, [-1, hidden_num])
#
#         W = tf.get_variable("w", shape=[hidden_num, hidden_num], initializer=tf.contrib.layers.xavier_initializer())
#         b = tf.get_variable('b', [hidden_num], initializer=tf.constant_initializer(0.1))
#         mlp_output = tf.nn.relu(tf.nn.xw_plus_b(state_exp, W, b))
#
#
#         output = tf.reshape(mlp_output, [-1, input_shape[1], hidden_num])
#
#     return output

# def drnn_mlp(input, windows_size, hidden_num, mlp_num, keep_prob, is_training, name):
#     #input_handel = tf.nn.dropout(tf.reshape(input, input_shape), keep_prob=keep_prob)
#     #　shape = tf.shape(input)
#     input_pad = tf.pad(input, [[0, 0], [windows_size - 1, 0], [0, 0]])
#     state = []
#
#     with tf.name_scope(name):
#         for i in range(input.shape[1]):
#             # seq_len = [windows_size if i < sentence_true_len else (0 if (i - sentence_true_len) >=  windows_size else windows_size - (i - sentence_true_len) - 1) for xx in sentence_true_len]
#             with tf.variable_scope('rnn') as scope:
#                 if i != 0:
#                     scope.reuse_variables()
#                 data_win = input_pad[:, i: i + windows_size - 1, :]
#                 cell = tf.contrib.rnn.DropoutWrapper(GRUCell(hidden_num),
#                                                      input_keep_prob=keep_prob[0],
#                                                      output_keep_prob=keep_prob[0])
#                 _, laststate = tf.nn.dynamic_rnn(cell, inputs=data_win, dtype=tf.float32)
#
#             laststate_bn = tf.layers.batch_normalization(laststate, axis=1, training=is_training, name="bn" + str(i))
#
#             W = tf.get_variable("w" + str(i), shape=[hidden_num, mlp_num], initializer=tf.contrib.layers.xavier_initializer())
#             b = tf.get_variable('b' + str(i), [mlp_num], initializer=tf.constant_initializer(0.1))
#             mlp_output = tf.nn.relu(tf.nn.xw_plus_b(laststate_bn, W, b))
#
#             state.append(tf.reshape(mlp_output, [-1, 1, mlp_num]))
#
#         state_con = tf.concat(state, axis=1)
#
#     return state_con

# def drnn_mlp(input, windows_size, hidden_num, mlp_num, keep_prob, is_training, name):
#     #input_handel = tf.nn.dropout(tf.reshape(input, input_shape), keep_prob=keep_prob)
#     # shape = tf.shape(input)
#     input_pad = tf.pad(input, [[0, 0], [windows_size - 1, 0], [0, 0]])
#     state = []
#
#     with tf.name_scope(name):
#         for i in range(input.shape[1].value):
#             # seq_len = [windows_size if i < sentence_true_len else (0 if (i - sentence_true_len) >=  windows_size else windows_size - (i - sentence_true_len) - 1) for xx in sentence_true_len]
#             with tf.variable_scope(name + 'rnn') as scope:
#                 if i != 0:
#                     scope.reuse_variables()
#                 data_win = input_pad[:, i: i + windows_size - 1, :]
#                 cell = tf.contrib.rnn.DropoutWrapper(GRUCell(hidden_num),
#                                                      input_keep_prob=keep_prob[0],
#                                                      output_keep_prob=keep_prob[0])
#                 _, laststate = tf.nn.dynamic_rnn(cell, inputs=data_win, dtype=tf.float32)
#
#                 laststate_bn = tf.layers.batch_normalization(laststate, axis=1, training=is_training)
#
#             state.append(tf.expand_dims(laststate_bn, 1))
#
#         state_con = tf.concat(state, axis=1)
#
#         W = tf.get_variable(name + "w", shape=[hidden_num, mlp_num],
#                             initializer=tf.contrib.layers.xavier_initializer())
#         b = tf.get_variable(name + 'b', [mlp_num], initializer=tf.constant_initializer(0.1))
#         mlp_output = tf.nn.relu(tf.nn.xw_plus_b(tf.reshape(state_con, [-1, hidden_num]), W, b))
#
#         mlp_handel = tf.reshape(mlp_output, [-1, input.shape[1].value, mlp_num])
#
#     return mlp_handel

def drnn_mlp_new(input, batch_size, sentence_length, windows_size, hidden_num, mlp_num, sentence_len_2d, keep_prob, is_training, name):
    input_pad = tf.pad(input, [[0, 0], [windows_size - 1, 0], [0, 0]])
    data_expand = []

    with tf.name_scope(name):
        for i in range(input.shape[1].value):
            data_win = input_pad[:, i: i + windows_size, :]
            data_expand.append(data_win)

        data_con = tf.concat(data_expand, axis=1)
        data_input = tf.reshape(data_con, [-1, windows_size, input.shape[2].value])
        sen_len = tf.reshape(sentence_len_2d, [-1])


        # cell = tf.contrib.rnn.DropoutWrapper(GRUCell(hidden_num),
        #                                         input_keep_prob=keep_prob[0],
        #                                         output_keep_prob=keep_prob[0])
        cell = GRUCell(hidden_num)
        state = cell.zero_state(batch_size * sentence_length, dtype=tf.float32)
        with tf.variable_scope("RNN"):
            for time in range(windows_size):
                if time > 0:
                    tf.get_variable_scope().reuse_variables()
                _, state = cell(tf.nn.dropout(data_input[:, time, :], keep_prob=keep_prob[0]), state)
                state = tf.nn.dropout(state, keep_prob=keep_prob[0])
        laststate = state


        #_, laststate = tf.nn.dynamic_rnn(cell, inputs=data_input, sequence_length=sen_len, dtype=tf.float32)

        laststate_bn = tf.layers.batch_normalization(laststate, axis=1, training=is_training, name=name + 'bn')

        W = tf.get_variable(name + "w", shape=[hidden_num, mlp_num],
                            initializer=tf.contrib.layers.xavier_initializer())
        b = tf.get_variable(name + 'b', [mlp_num], initializer=tf.constant_initializer(0.1))
        mlp_output = tf.nn.relu(tf.nn.xw_plus_b(laststate_bn, W, b))

        mlp_handel = tf.reshape(mlp_output, [-1, input.shape[1].value, mlp_num])

    return mlp_handel

def drnn_mlp(input, windows_size, hidden_num, mlp_num, sentence_len_2d, keep_prob, is_training, name):
    input_pad = tf.pad(input, [[0, 0], [windows_size - 1, 0], [0, 0]])
    data_expand = []

    with tf.name_scope(name):
        for i in range(input.shape[1].value):
            data_win = input_pad[:, i: i + windows_size, :]
            data_expand.append(data_win)

        data_con = tf.concat(data_expand, axis=1)
        data_input = tf.reshape(data_con, [-1, windows_size, input.shape[2].value])
        sen_len = tf.reshape(sentence_len_2d, [-1])


        cell = tf.contrib.rnn.DropoutWrapper(GRUCell(hidden_num),
                                                input_keep_prob=keep_prob[0],
                                                output_keep_prob=keep_prob[0])
        _, laststate = tf.nn.dynamic_rnn(cell, inputs=data_input, sequence_length=sen_len, dtype=tf.float32)

        laststate_bn = tf.layers.batch_normalization(laststate, axis=1, training=is_training, name=name + 'bn')

        W = tf.get_variable(name + "w", shape=[hidden_num, mlp_num],
                            initializer=tf.contrib.layers.xavier_initializer())
        b = tf.get_variable(name + 'b', [mlp_num], initializer=tf.constant_initializer(0.1))
        W2 = tf.get_variable(name + "w2", shape=[mlp_num, hidden_num],
                            initializer=tf.contrib.layers.xavier_initializer())
        b2 = tf.get_variable(name + 'b2', [hidden_num], initializer=tf.constant_initializer(0.1))
        mlp_output = tf.nn.relu(tf.nn.xw_plus_b(laststate_bn, W, b))
        mlp_output2 = tf.nn.relu(tf.nn.xw_plus_b(mlp_output, W2, b2))

        mlp_handel = tf.reshape(mlp_output2, [-1, input.shape[1].value, hidden_num])

    return mlp_handel

def drnn_layer(input, windows_size, hidden_num, keep_prob, is_training, name):
    input_pad = tf.pad(input, [[0, 0], [windows_size - 1, 0], [0, 0]])
    data_expand = []

    if not is_training:
        keep_prob = 1
    with tf.name_scope(name):
        for i in range(input.shape[1].value):
            data_win = input_pad[:, i: i + windows_size, :]
            data_expand.append(data_win)

        data_con = tf.concat(data_expand, axis=1)
        data_input = tf.reshape(data_con, [-1, windows_size, input.shape[2].value])
        # sen_len = tf.reshape(sentence_len_2d, [-1])


        cell = tf.contrib.rnn.DropoutWrapper(GRUCell(hidden_num),
                                                input_keep_prob=keep_prob,
                                                output_keep_prob=keep_prob)
        # sen_len = tf.constant(windows_size, shape=[data_input.shape[0].value], dtype=tf.int32)
        hidden_state, laststate = tf.nn.dynamic_rnn(cell, inputs=data_input, dtype=tf.float32)

        # laststate_bn = tf.layers.batch_normalization(laststate, axis=1, training=is_training, name=name + 'bn')

        # W = tf.get_variable(name + "w", shape=[hidden_num, mlp_num],
        #                     initializer=tf.contrib.layers.xavier_initializer())
        # b = tf.get_variable(name + 'b', [mlp_num], initializer=tf.constant_initializer(0.1))
        # mlp_output = tf.nn.relu(tf.nn.xw_plus_b(laststate_bn, W, b))

        state_handel = tf.reshape(laststate, [-1, input.shape[1].value, hidden_num])

    return state_handel