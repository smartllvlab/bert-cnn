一、BERT-CNN模型的训练和测试

1、使用Anaconda 3 和Tensorflow 1.12 以上版本

2、需要GPU内存11G以上

3、训练过程(自行替换父路径)：

在运行训练程序之前需要设置路径：
export GLUE_DIR=父路径/BERT-CNN/glue_data
export BERT_BASE_DIR=父路径/BERT-CNN/uncased_L-12_H-768_A-12
cd 父路径/BERT-CNN

AG数据集：
python run_classifier.py   --task_name=ag   --do_train=true   --do_eval=true   --data_dir=$GLUE_DIR/AG   --vocab_file=$BERT_BASE_DIR/vocab.txt   --bert_config_file=$BERT_BASE_DIR/bert_config.json   --init_checkpoint=$BERT_BASE_DIR/bert_model.ckpt   --max_seq_length=128   --train_batch_size=32   --learning_rate=2e-5   --num_train_epochs=3.0   --output_dir=父路径/BERT-CNN/model_save/ag_output/

SST-1数据集：
python run_classifier.py   --task_name=sst1   --do_train=true   --do_eval=true   --data_dir=$GLUE_DIR/SST-1   --vocab_file=$BERT_BASE_DIR/vocab.txt   --bert_config_file=$BERT_BASE_DIR/bert_config.json   --init_checkpoint=$BERT_BASE_DIR/bert_model.ckpt   --max_seq_length=128   --train_batch_size=32   --learning_rate=5e-6   --num_train_epochs=5.0   --output_dir=父路径/BERT-CNN/model_save/sst1_output/

SST-2数据集：
python run_classifier.py   --task_name=sst2ori   --do_train=true   --do_eval=true   --data_dir=$GLUE_DIR/SST-2-ori   --vocab_file=$BERT_BASE_DIR/vocab.txt   --bert_config_file=$BERT_BASE_DIR/bert_config.json   --init_checkpoint=$BERT_BASE_DIR/bert_model.ckpt   --max_seq_length=128   --train_batch_size=32   --learning_rate=1e-5   --num_train_epochs=5.0   --output_dir=父路径/BERT-CNN/model_save/sst2_output/

Yelp-F数据集：
python run_classifier.py   --task_name=yelp_f   --do_train=true   --do_eval=true   --data_dir=$GLUE_DIR/Yelp_F   --vocab_file=$BERT_BASE_DIR/vocab.txt   --bert_config_file=$BERT_BASE_DIR/bert_config.json   --init_checkpoint=$BERT_BASE_DIR/bert_model.ckpt   --max_seq_length=150   --train_batch_size=32   --learning_rate=2e-5   --num_train_epochs=3.0   --output_dir=父路径/BERT-CNN/model_save/yelp_output/

4、根据文章结果的模型文件进行测试：

AG数据集：
python run_classifier.py   --task_name=ag   --do_train=false   --do_eval=true   --data_dir=$GLUE_DIR/AG   --vocab_file=$BERT_BASE_DIR/vocab.txt   --bert_config_file=$BERT_BASE_DIR/bert_config.json   --init_checkpoint=$BERT_BASE_DIR/bert_model.ckpt   --max_seq_length=128   --train_batch_size=32   --learning_rate=2e-5   --num_train_epochs=3.0   --output_dir=父路径/BERT-CNN/model_save/ag_output-2/

SST-1数据集：
python run_classifier.py   --task_name=sst1   --do_train=false   --do_eval=true   --data_dir=$GLUE_DIR/SST-1   --vocab_file=$BERT_BASE_DIR/vocab.txt   --bert_config_file=$BERT_BASE_DIR/bert_config.json   --init_checkpoint=$BERT_BASE_DIR/bert_model.ckpt   --max_seq_length=128   --train_batch_size=32   --learning_rate=5e-6   --num_train_epochs=5.0   --output_dir=父路径/BERT-CNN/model_save/sst1_output-5/

SST-2数据集：
python run_classifier.py   --task_name=sst2ori   --do_train=false   --do_eval=true   --data_dir=$GLUE_DIR/SST-2-ori   --vocab_file=$BERT_BASE_DIR/vocab.txt   --bert_config_file=$BERT_BASE_DIR/bert_config.json   --init_checkpoint=$BERT_BASE_DIR/bert_model.ckpt   --max_seq_length=128   --train_batch_size=32   --learning_rate=1e-5   --num_train_epochs=5.0   --output_dir=父路径/BERT-CNN/model_save/sst2_output-3/

Yelp-F数据集：
python run_classifier.py   --task_name=yelp_f   --do_train=false   --do_eval=true   --data_dir=$GLUE_DIR/Yelp_F   --vocab_file=$BERT_BASE_DIR/vocab.txt   --bert_config_file=$BERT_BASE_DIR/bert_config.json   --init_checkpoint=$BERT_BASE_DIR/bert_model.ckpt   --max_seq_length=150   --train_batch_size=32   --learning_rate=2e-5   --num_train_epochs=3.0   --output_dir=父路径/BERT-CNN/model_save/yelp_output-4/

二、combine结果：

1、先输出测试结果：
AG数据集：
python run_classifier.py   --task_name=ag   --do_train=false   --do_eval=false  --do_predict=true  --data_dir=$GLUE_DIR/AG   --vocab_file=$BERT_BASE_DIR/vocab.txt   --bert_config_file=$BERT_BASE_DIR/bert_config.json   --init_checkpoint=$BERT_BASE_DIR/bert_model.ckpt   --max_seq_length=128   --train_batch_size=32   --learning_rate=2e-5   --num_train_epochs=3.0   --output_dir=父路径/BERT-CNN/model_save/ag_output-2/

SST-1数据集：
python run_classifier.py   --task_name=sst1   --do_train=false   --do_eval=false   --do_predict=true  --data_dir=$GLUE_DIR/SST-1   --vocab_file=$BERT_BASE_DIR/vocab.txt   --bert_config_file=$BERT_BASE_DIR/bert_config.json   --init_checkpoint=$BERT_BASE_DIR/bert_model.ckpt   --max_seq_length=128   --train_batch_size=32   --learning_rate=5e-6   --num_train_epochs=5.0   --output_dir=父路径/BERT-CNN/model_save/sst1_output-5/

SST-2数据集：
python run_classifier.py   --task_name=sst2ori   --do_train=false   --do_eval=false   --do_predict=true  --data_dir=$GLUE_DIR/SST-2-ori   --vocab_file=$BERT_BASE_DIR/vocab.txt   --bert_config_file=$BERT_BASE_DIR/bert_config.json   --init_checkpoint=$BERT_BASE_DIR/bert_model.ckpt   --max_seq_length=128   --train_batch_size=32   --learning_rate=1e-5   --num_train_epochs=5.0   --output_dir=父路径/BERT-CNN/model_save/sst2_output-3/

Yelp-F数据集：
python run_classifier.py   --task_name=yelp_f   --do_train=false   --do_eval=false   --do_predict=true  --data_dir=$GLUE_DIR/Yelp_F   --vocab_file=$BERT_BASE_DIR/vocab.txt   --bert_config_file=$BERT_BASE_DIR/bert_config.json   --init_checkpoint=$BERT_BASE_DIR/bert_model.ckpt   --max_seq_length=150   --train_batch_size=32   --learning_rate=2e-5   --num_train_epochs=3.0   --output_dir=父路径/BERT-CNN/model_save/yelp_output-4/

2、combine程序：

cd 父路径/BERT-CNN/combine

AG数据集：
python AG_combine.py

SST-1数据集：
python SST1_combine

SST-2数据集：
python SST2_combine.py

Yelp-F数据集：
python Yelp_F_combine.py

结果W为BERT-CNN权重（1-W为BERT权重）
       A为最高的准确率