import numpy as np

file_train = open('train.txt', 'w', encoding='utf-8')
file_test = open('test.txt', 'w', encoding='utf-8')

file = open('rt-polaritydata/rt-polarity.pos', 'r', encoding='utf-8', errors='ignore')
file2 = open('rt-polaritydata/rt-polarity.neg', 'r', encoding='utf-8', errors='ignore')

for line in file:
    if np.random.rand() < 0.8:
        file_train.write(line.strip() + '\t1\n')
    else:
        file_test.write(line.strip() + '\t1\n')
for line in file2:
    if np.random.rand() < 0.8:
        file_train.write(line.strip() + '\t0\n')
    else:
        file_test.write(line.strip() + '\t0\n')

file_train.close()
file_test.close()
